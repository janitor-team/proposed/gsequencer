/* GSequencer - Advanced GTK Sequencer
 * Copyright (C) 2005-2021 Joël Krähemann
 *
 * This file is part of GSequencer.
 *
 * GSequencer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GSequencer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GSequencer.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <ags/app/editor/ags_machine_selector_callbacks.h>

#include <ags/app/ags_ui_provider.h>
#include <ags/app/ags_composite_editor.h>
#include <ags/app/ags_notation_editor.h>

#include <ags/app/editor/ags_machine_selection.h>
#include <ags/app/editor/ags_machine_radio_button.h>

void ags_machine_selector_selection_response(GtkWidget *machine_selection,
					     gint response,
					     AgsMachineSelector *machine_selector);

void
ags_machine_selector_popup_add_tab_callback(GtkWidget *menu_item, AgsMachineSelector *machine_selector)
{
  //TODO:JK: implement me
}

void
ags_machine_selector_popup_remove_tab_callback(GtkWidget *menu_item, AgsMachineSelector *machine_selector)
{
  //TODO:JK: implement me
}

void
ags_machine_selector_popup_add_index_callback(GtkWidget *menu_item, AgsMachineSelector *machine_selector)
{
  ags_machine_selector_add_index(machine_selector);
}

void
ags_machine_selector_popup_remove_index_callback(GtkWidget *menu_item, AgsMachineSelector *machine_selector)
{
  GList *list, *list_start;

  guint nth;
  
  /* find index */
  list_start = 
    list = gtk_container_get_children(GTK_CONTAINER(machine_selector));
  list = list->next;

  nth = 0;
  
  while(list != NULL){

    if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(list->data))){
      ags_machine_selector_changed(machine_selector,
				   NULL);

      break;
    }

    list = list->next;
    nth++;
  }

  g_list_free(list_start);

  /* remove index */
  ags_machine_selector_remove_index(machine_selector,
				    nth);
}

void
ags_machine_selector_popup_link_index_callback(GtkWidget *menu_item, AgsMachineSelector *machine_selector)
{
  AgsWindow *window;
  AgsMachine *machine;
  AgsMachineSelection *machine_selection;
  AgsMachineRadioButton *machine_radio_button;

  GList *list;
  
  list = gtk_window_list_toplevels();

  while(list != NULL && !AGS_IS_WINDOW(list->data)) list = list->next;

  if(list == NULL){
    return;
  }

  window = list->data;

  machine_selection = (AgsMachineSelection *) ags_machine_selection_new(window);
  machine_selector->machine_selection = (GtkDialog *) machine_selection;

  if((AGS_MACHINE_SELECTOR_EDIT_NOTATION & (machine_selector->edit)) != 0){
    ags_machine_selection_set_edit(machine_selection, AGS_MACHINE_SELECTION_EDIT_NOTATION);
  }

  if((AGS_MACHINE_SELECTOR_EDIT_AUTOMATION & (machine_selector->edit)) != 0){
    ags_machine_selection_set_edit(machine_selection, AGS_MACHINE_SELECTION_EDIT_AUTOMATION);
  }

  if((AGS_MACHINE_SELECTOR_EDIT_WAVE & (machine_selector->edit)) != 0){
    ags_machine_selection_set_edit(machine_selection, AGS_MACHINE_SELECTION_EDIT_WAVE);
  }
  
  ags_machine_selection_load_defaults(machine_selection);
  g_signal_connect(G_OBJECT(machine_selection), "response",
		   G_CALLBACK(ags_machine_selector_selection_response), machine_selector);
  gtk_widget_show_all((GtkWidget *) machine_selection);
}

void
ags_machine_selector_selection_response(GtkWidget *machine_selection,
					gint response,
					AgsMachineSelector *machine_selector)
{
  AgsMachine *machine;
  GtkVBox *vbox;
  GtkContainer *content_area;

  GList *list, *list_start;

  if(response == GTK_RESPONSE_ACCEPT){
    /* retrieve machine */
    machine = NULL;
    vbox = (GtkVBox *) gtk_dialog_get_content_area(GTK_DIALOG(machine_selection));

    if(response == GTK_RESPONSE_ACCEPT){
      list_start =
	list = gtk_container_get_children((GtkContainer *) vbox);

      while(list != NULL){
	if(GTK_IS_TOGGLE_BUTTON(list->data) &&
	   gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(list->data))){
	  machine = g_object_get_data(list->data,
				      AGS_MACHINE_SELECTION_INDEX);
	  
	  break;
	}
	
	list = list->next;
      }

      g_list_free(list_start);
    }

    /* link index  */
    ags_machine_selector_link_index(machine_selector,
				    machine);
  }

  /* unset machine selection and destroy */
  machine_selector->machine_selection = NULL;

  gtk_widget_destroy(machine_selection);
}

void
ags_machine_selector_radio_changed(GtkWidget *radio_button, AgsMachineSelector *machine_selector)
{
  ags_machine_selector_changed(machine_selector, AGS_MACHINE_RADIO_BUTTON(radio_button)->machine);
}

void
ags_machine_selector_popup_reverse_mapping_callback(GtkWidget *menu_item, AgsMachineSelector *machine_selector)
{
  AgsMachine *machine;
  
  AgsApplicationContext *application_context;
  
  gboolean use_composite_editor;

  if((AGS_MACHINE_SELECTOR_BLOCK_REVERSE_MAPPING & (machine_selector->flags)) != 0){
    return;
  }

  application_context = ags_application_context_get_instance();

  use_composite_editor = ags_ui_provider_use_composite_editor(AGS_UI_PROVIDER(application_context));

  if(use_composite_editor){
    AgsCompositeEditor *composite_editor;
    
    composite_editor = gtk_widget_get_ancestor(GTK_WIDGET(machine_selector),
					       AGS_TYPE_COMPOSITE_EDITOR);

    machine = composite_editor->selected_machine;
  }else{
    AgsNotationEditor *notation_editor;
    
    notation_editor = gtk_widget_get_ancestor(GTK_WIDGET(machine_selector),
					      AGS_TYPE_NOTATION_EDITOR);

    machine = notation_editor->selected_machine;
  }
  
  if(machine != NULL){
    if(gtk_check_menu_item_get_active((GtkCheckMenuItem *) menu_item)){
      ags_audio_set_behaviour_flags(machine->audio,
				    AGS_SOUND_BEHAVIOUR_REVERSE_MAPPING);
    }else{
      ags_audio_unset_behaviour_flags(machine->audio,
				      AGS_SOUND_BEHAVIOUR_REVERSE_MAPPING);
    }
  }
}

void
ags_machine_selector_popup_shift_piano_callback(GtkWidget *menu_item, AgsMachineSelector *machine_selector)
{
  AgsMachine *machine;
  AgsPiano *piano;
  AgsNotationEdit *notation_edit;
  
  AgsApplicationContext *application_context;
  
  gchar *base_note;
  gchar *label;

  gint base_key_code;
  gboolean use_composite_editor;

  application_context = ags_application_context_get_instance();

  use_composite_editor = ags_ui_provider_use_composite_editor(AGS_UI_PROVIDER(application_context));

  if(use_composite_editor){
    AgsCompositeEditor *composite_editor;
    
    composite_editor = gtk_widget_get_ancestor(GTK_WIDGET(machine_selector),
					       AGS_TYPE_COMPOSITE_EDITOR);

    machine = composite_editor->selected_machine;

    notation_edit = composite_editor->notation_edit->edit;
    
    piano = AGS_SCROLLED_PIANO(composite_editor->notation_edit->edit_control)->piano;
  }else{
    AgsNotationEditor *notation_editor;
    
    notation_editor = gtk_widget_get_ancestor(GTK_WIDGET(machine_selector),
					      AGS_TYPE_NOTATION_EDITOR);

    machine = notation_editor->selected_machine;

    notation_edit = notation_editor->notation_edit;
    
    piano = notation_editor->scrolled_piano->piano;
  }
  
  label = gtk_menu_item_get_label((GtkMenuItem *) menu_item);

  base_note = NULL;
  base_key_code = 0;
    
  if(!g_strcmp0(label,
		"A")){
    base_note = AGS_PIANO_KEYS_OCTAVE_2_A;
    base_key_code = 33;
  }else if(!g_strcmp0(label,
		      "A#")){
    base_note = AGS_PIANO_KEYS_OCTAVE_2_AIS;
    base_key_code = 34;
  }else if(!g_strcmp0(label,
		      "H")){
    base_note = AGS_PIANO_KEYS_OCTAVE_2_H;
    base_key_code = 35;
  }else if(!g_strcmp0(label,
		      "C")){
    base_note = AGS_PIANO_KEYS_OCTAVE_2_C;
    base_key_code = 24;
  }else if(!g_strcmp0(label,
		      "C#")){
    base_note = AGS_PIANO_KEYS_OCTAVE_2_CIS;
    base_key_code = 25;
  }else if(!g_strcmp0(label,
		      "D")){
    base_note = AGS_PIANO_KEYS_OCTAVE_2_D;
    base_key_code = 26;
  }else if(!g_strcmp0(label,
		      "D#")){
    base_note = AGS_PIANO_KEYS_OCTAVE_2_DIS;
    base_key_code = 27;
  }else if(!g_strcmp0(label,
		      "E")){
    base_note = AGS_PIANO_KEYS_OCTAVE_2_E;
    base_key_code = 28;
  }else if(!g_strcmp0(label,
		      "F")){
    base_note = AGS_PIANO_KEYS_OCTAVE_2_F;
    base_key_code = 29;
  }else if(!g_strcmp0(label,
		      "F#")){
    base_note = AGS_PIANO_KEYS_OCTAVE_2_FIS;
    base_key_code = 30;
  }else if(!g_strcmp0(label,
		      "G")){
    base_note = AGS_PIANO_KEYS_OCTAVE_2_G;
    base_key_code = 31;
  }else if(!g_strcmp0(label,
		      "G#")){
    base_note = AGS_PIANO_KEYS_OCTAVE_2_GIS;
    base_key_code = 32;
  }

  g_object_set(piano,
	       "base-note", base_note,
	       "base-key-code", base_key_code,
	       NULL);
  
  if(machine != NULL){
    g_free(machine->base_note);
    
    machine->base_note = g_strdup(base_note);
    machine->base_key_code = base_key_code;
  }
  
  gtk_widget_queue_draw((GtkWidget *) piano);
  gtk_widget_queue_draw((GtkWidget *) notation_edit);
}
